function makeZoomOfic() {
$('.to_zoom').jqzoom({
  zoomType: 'innerzoom',
  zoomWidth: 300,
  zoomHeight: 250,
  title: false
});
}


function setAnchor() {
  // hide #back-top first
  $("#back-top").hide();
   // fade in #back-top
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('#back-top').fadeIn();
      } else {
        $('#back-top').fadeOut();
      }
    });

    // scroll body to 0px on click
    $('#back-top, #back-top a').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 1500);
      return false;
    });
  });
}

function ScrollSpyMenu() {
    var offset = 126,
        menuItem = $('.b-navbar_nav li a');
    $('body').scrollspy({ target: '.b-navbar', offset:offset});
    menuItem.each(function(){
       $(this).on('click', function(e){
           var e = e || window.event,
               link,
               anchor,
               anchorOffset;
          e.preventDefault();
          link = $(this).attr('href');
          anchor = $(document).find(link);
          anchorOffset = anchor.offset().top;
           $('body,html').animate({
               scrollTop: anchorOffset - 126
           }, 500);
           location.hash = link;
       });
    });
}