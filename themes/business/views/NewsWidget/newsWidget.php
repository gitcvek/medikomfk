<?php
$this->registerCssFile('newsWidget.css');
?>
<div class="b-news-widget">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="title-lg"><span>События</span></h3>
        </div>
    </div> <!-- / .row -->
  <?php
  //если нужно брать переопределенное представление элемента списка из темы,
  //то вместо news.views._list_item нужно прописать
  //webroot.themes.название_темы.views.news._list_item
  ?>
<?php foreach ($this->getNews() as $model): ?>
<?php $this->render('webroot.themes.business.views.news._list_item', array('model' => $model)); ?>
<?php endforeach; ?>
<div class="archive text-right"><a class="btn btn-primary btn-xs" href="<?php echo Yii::app()->createUrl(NewsModule::ROUTE_NEWS_CATEGORY);?>">Все новости &nbsp;»</a></div>
</div>