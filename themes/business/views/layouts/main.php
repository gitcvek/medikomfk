<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="content-language" content="ru"> <?php // TODO - в будущем генетить автоматом ?>
    <?php
    //Регистрируем файлы скриптов в <head>
    if (YII_DEBUG) {
        Yii::app()->assetManager->publish(YII_PATH . '/web/js/source', false, -1, true);
    }
    Yii::app()->clientScript->registerCoreScript('jquery');
    $this->registerJsFile('bootstrap.min.js', 'ygin.assets.bootstrap.js');
    $this->registerJsFile('modernizr-2.6.1-respond-1.1.0.min.js', 'ygin.assets.js');

    Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);
    Yii::app()->clientScript->registerScript('scrollSpy.init', "ScrollSpyMenu();", CClientScript::POS_READY);


    $this->registerCssFile('bootstrap.min.css', 'application.assets.bootstrap.css');
    $ass = Yii::getPathOfAlias('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
        $ass . 'glyphicons-halflings-regular.eot' => '../fonts/',
        $ass . 'glyphicons-halflings-regular.svg' => '../fonts/',
        $ass . 'glyphicons-halflings-regular.woff' => '../fonts/',
        $ass . 'glyphicons-halflings-regular.ttf' => '../fonts/',
    ));

    $this->registerCssFile('font-awesome.min.css', 'application.assets.font-awesome.css');
    $ass2 = Yii::getPathOfAlias('application.assets.font-awesome.fonts') . DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('font-awesome.min.css', array(
        $ass2 . 'fontawesome-webfont.eot' => '../fonts/',
        $ass2 . 'fontawesome-webfont.svg' => '../fonts/',
        $ass2 . 'fontawesome-webfont.woff' => '../fonts/',
        $ass2 . 'fontawesome-webfont.ttf' => '../fonts/',
    ));

    Yii::app()->clientScript->registerCssFile('/themes/business/css/animate.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/style.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
    ?>
    <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body data-spy="scroll">
<div class="b-navbar navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="/themes/business/gfx/logo.png" alt=""/>
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <div class="tright pull-right">
                <div class="numbers">
                    <p class="phone"><strong><?php echo Yii::app()->params['phone_number']; ?></strong></p>
                </div>
            </div>
            <?php
            $this->widget('MenuWidget', array(
                'rootItem' => Yii::app()->menu->all,
                'htmlOptions' => array('class' => 'b-navbar_nav nav navbar-nav navbar-right'), // корневой ul
                'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
                'activeCssClass' => 'active', // активный li
                'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
                //'labelTemplate' => '{label}', // шаблон для подписи
                'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
                //'linkOptions' => array(), // атрибуты для ссылок
                'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
                //'itemOptions' => array(), // атрибуты для li
                'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
                'maxChildLevel' => 1,
                'encodeLabel' => false,
            )); ?>
            <!-- Mobile Search -->
            <!--<form class="navbar-form navbar-right visible-xs" role="search">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-btn">
                  <button class="btn btn-red" type="button">Search!</button>
                </span>
              </div>
            </form>-->
        </div><!--/.nav-collapse -->
    </div>
</div> <!-- / .navigation -->

<div class="wrapper">
    <div class="container">
        <div class="row">
            <!-- Key Features -->
            <div id="about" class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="title-lg"><span>О компании</span></h1>
                    </div>
                </div> <!-- / .row -->
                <div class="row feature_block2">
                    <div class="col-sm-8">
                        <p>
                            Деятельность фармацевтической компании &laquo;Медиком&raquo; направлена на
                            обеспечение населения Республики Коми и близлежащих регионов
                            медицинской продукцией с целью улучшения здравоохранения и уровня жизни людей.
                        </p>
                        <p>
                            Приоритетами деятельности компании является быстрое бесперебойное
                            удовлетворение потребностей покупателей в медицинских товарах при
                            обеспечении их надлежащего качества и по разумным ценам.
                        </p>

                        <p>
                            Фармацевтическая компания &laquo;Медиком&raquo; осознает свою социальную ответственность
                            в сфере здравоохранения и предоставляет специальные скидки социально незащищённым группам
                            населения,
                            оказывает материальную помощь больницам, детям и общественным организациям.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="feedback" class="row">
            <div class="col-sm-12">
                <h1 class="title-lg"><span>Обратная связь</span></h1>
            </div>
        </div>
        <div class="row portfolio-block">
            <div class="col-sm-8 col-xs-12">
                <?php $this->widget('feedback.widgets.FeedbackWidget'); ?>
            </div>
            <div class="col-sm-4 col-xs-12">
                <h4>С нами можно связаться:</h4>

                <p>Юридический адрес организации:г.Сыктывкар, Покровский бульвар, д.1</p>

                <p>по телефону <?php echo Yii::app()->params['phone_number']; ?>
                    , <?php echo Yii::app()->params['phone']; ?></p>

                <p>по электронной почте <a href="mailto:medikom@inbox.ru">medikom@inbox.ru</a></p>

            </div>
        </div>
        <div id="requisites">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="title-lg"><span>Реквизиты</span></h1>
                </div>
                <div class="portfolio-block clearfix">
                    <div class="col-sm-6 col-xs-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2">Общество с ограниченной ответственностью «ФК МЕДИКОМ»</th>
                            </tr>
                            </thead>
                            <tr>
                                <td>ИНН</td>
                                <td>1101123378</td>
                            </tr>
                            <tr>
                                <td>КПП</td>
                                <td>110101001</td>
                            </tr>
                            <tr>
                                <td>ОГРН</td>
                                <td>1051100559795 от 19.10.2005 г. серия 11 № 001072109</td>
                            </tr>
                            <tr>
                                <td>ОКПО</td>
                                <td>77896449</td>
                            </tr>
                            <tr>
                                <td>ОКВЭД</td>
                                <td>51.46, 52.3</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2">Банковские реквизиты</th>
                            </tr>
                            </thead>
                            <tr>
                                <td>Банк</td>
                                <td>Отделение № 8617 Сбербанка России</td>
                            </tr>
                            <tr>
                                <td>БИК</td>
                                <td>048702640</td>
                            </tr>
                            <tr>
                                <td>Расчетный счет</td>
                                <td>40702810928000008306</td>
                            </tr>
                            <tr>
                                <td>Корр. Счет</td>
                                <td>30101810400000000640</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div> <!-- / .container -->


</div> <!-- / .wrapper -->
<div id="footer" class="container">
    <div class="row">
        <div class="col-lg-2 col-lg-offset-10">
            <div id="cvek"><a title="создать сайт в Цифровом веке" href="http://cvek.ru">Создание сайта —
                    веб-студия &laquo;Цифровой век&raquo;</a></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    WebFontConfig = {
        google: {families: ['Open+Sans:400,300,600,700,800:latin,cyrillic']}
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })(); </script>
</body>
</html>