<div class="b-feedback-widget">
<!--  <button class="btn btn-primary btn-lg" onclick="$('#feedbackForm').modal()"><span class="glyphicon glyphicon-comment"></span> Написать сообщение</button>-->
<?php

if (Yii::app()->user->hasFlash('feedback-success')) {
  $this->widget('AlertWidget', array(
    'title' => 'Обратная связь',
    'message' => Yii::app()->user->getFlash('feedback-success'),
  ));
}
    
$this->registerCssFile('b-feedback-widget.css');

$hidden = 'none';
if (Yii::app()->user->hasFlash("feedback-message")) {
  $hidden = 'block';
  //echo '<i>'.Yii::app()->user->getFlash('feedback-message').'</i>';
}

/**
 * @var $form CActiveForm
 */
$form = $this->beginWidget('CActiveForm', array(
  'id' => 'feedbackForm',
  'enableAjaxValidation' => false,
  'enableClientValidation' => true,
  //'focus' => array($model, 'fio'),
  'htmlOptions' => array(
    'class' => '',
    'role' => 'form',
    //'style' => 'display:'.$hidden.';',
  ),
  'clientOptions' => array(
    'validateOnSubmit' => true,
    'validateOnChange' => false,
  ),
  'errorMessageCssClass' => 'label label-danger',
  'action' => Yii::app()->createUrl(FeedbackModule::ROUTE_FEEDBACK),
));

?>
    <?php echo Yii::app()->user->getFlash("feedback-message"); ?>
    <fieldset>
      <div class="form-group">
        <?php echo $form->labelEx($model, 'fio', array('class'=>'control-label')); ?>
          <?php echo $form->textField($model, 'fio', array('class' => 'form-control focused', 'autocomplete' => 'off')); ?>
          <?php echo $form->error($model, 'fio'); ?>
      </div>
      <div class="form-group">
        <?php echo $form->labelEx($model, 'phone', array('class'=>'control-label')); ?>
          <?php echo $form->textField($model, 'phone', array('class' => 'form-control', 'autocomplete' => 'off', 'type' => 'tel')); ?>
          <?php echo $form->error($model, 'phone'); ?>
      </div>
      <div class="form-group">
        <?php echo $form->labelEx($model, 'mail', array('class'=>'control-label')); ?>
          <?php echo $form->textField($model, 'mail', array('class' => 'form-control', 'autocomplete' => 'off', 'type' => 'email')); ?>
          <?php echo $form->error($model, 'mail'); ?>
      </div>
      <div class="form-group">
        <?php echo $form->labelEx($model, 'message', array('class'=>'control-label')); ?>
          <?php echo $form->textArea($model, 'message', array('class'=>'form-control', 'rows' => '4', 'autocomplete' => 'off')); ?>
          <?php echo $form->error($model, 'message'); ?>
      </div>
      <div class="form-group row">
        <?php echo $form->labelEx($model, 'verifyCode', array('class'=>'control-label col-sm-12')); ?>
        <div class="col-sm-4">
          <?php echo $form->textField($model, 'verifyCode', array('class'=>'form-control', 'title' => 'Укажите код с картинки', 'autocomplete' => 'off')); ?>
        </div>
        <div class="col-sm-6">
        <?php $this->widget('CCaptcha', array('clickableImage' => true, 'captchaAction' => FeedbackModule::ROUTE_FEEDBACK_CAPTCHA)); ?>
        <?php echo $form->error($model, 'verifyCode', array(), true, false);?>
        </div>
      </div>
    </fieldset>
    <?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-lg btn-primary pull-right')); ?>
<?php $this->endWidget(); ?>
</div>